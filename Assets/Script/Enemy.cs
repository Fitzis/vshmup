﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour, IDamageable {
    Rigidbody2D rb;
    AudioSource audio;
    public enum ShootType { single,burst,radial};

    public float maxHealth=20.0f;
    protected float health;

    float shipHeight;
    float shipWidth;

    //Parametri di configurazione nemico
    public float speed = 5;
    public float shootDelay = 0.1f;
    public ShootType shootType = ShootType.single;
    public float bulletDamage = 10f;
    public float scoreValue = 100f;

    //Parametri configurazione movimento
    public Vector3[] path;

    //Parametri configurazione proiettili
    public Bullet.BulletPattern bulletPattern;
    public float bulletSpeed;
    public GameObject[] bulletSpawningPos;
    bool shootingRadial;

    //Parametri per sparo radiale o Burst
    protected float numBullet = 8;
    protected float burstAngle = 30;

    //Oggetti da spawnare
    public GameObject bullet;
    public GameObject explosion;
    public GameObject hitExplosion;


    // Use this for initialization

    protected void Start () {
        rb = GetComponent<Rigidbody2D>();
        StartMoving();

        SetHealth();

        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        shipHeight = sr.bounds.size.y;
        shipWidth = sr.bounds.size.x;

        audio = GetComponent<AudioSource>();

        //ResetOffset();
    }
    /*
    void ResetOffset() {
        bulletOffset.Clear();
        if (bulletSpawningPos.Length == 0) {//Se non abbiamo specificato dei punti da cui sparare
            bulletOffset.Add(-transform.up * shipHeight / 2);
        }
        else {
            for (int i = 0; i < bulletSpawningPos.Length; i++) {
                bulletOffset.Add(bulletSpawningPos[i].transform.position - transform.position);
            }
        }
    }*/

    Vector3 GetBulletOffset(int i) {
        if (shootType == ShootType.radial) {//Per lo sparo radiale non vogliamo che esca dal cannone ma dal centro
            return -transform.up * shipHeight / 2;
        }
        else {
            return bulletSpawningPos[i].transform.position - transform.position;
        }
    }

    protected void SetHealth() {
        GameManager.instance.CurrMaxHealth += maxHealth;
        GameManager.instance.CurrHealth += maxHealth;
        health = maxHealth;
    }

#region Sezione per sparare
    protected void StartShooting()
    {
        switch (shootType)
        {
            case ShootType.single:
                StartCoroutine("ShootSingle");
                break;
            case ShootType.burst:
                StartCoroutine("ShootBurst");
                break;
            case ShootType.radial:
                StartCoroutine("ShootRadial");
                break;
        }
    }

    protected void StopShooting() {
        switch (shootType) {
            case ShootType.single:
                StopCoroutine("ShootSingle");
                break;
            case ShootType.burst:
                StopCoroutine("ShootBurst");
                break;
            case ShootType.radial:
               // ResetOffset();
                StopCoroutine("ShootRadial");
                break;
        }
    }

    protected IEnumerator ShootSingle()
    {
        while (true)
        {
            ShootBullet();
            yield return new WaitForSeconds(shootDelay);
        }
    }

    protected IEnumerator ShootRadial()
    {

        while (true)
        {
            for(int i = 0; i < numBullet; i++)
            {
                Bullet[] b = ShootBullet();//TODO: correggere per avere tutti i proiettili
                for (int j = 0; j < bulletSpawningPos.Length; j++) {
                    b[j].customAngle = 360 / numBullet * i;
                }
            }
            yield return new WaitForSeconds(shootDelay);
        }
    }

    protected IEnumerator ShootBurst()
    {
        while (true)
        {
            for(int i=0; i < numBullet; i++)
            {
                Bullet[] b = ShootBullet();
                for(int j = 0; j < b.Length; j++) {
                    b[j].customAngle = -burstAngle / 2 + burstAngle / numBullet * i;
                }
                yield return new WaitForSeconds(0.05f);
            }
            yield return new WaitForSeconds(shootDelay);
        }
    }


    protected Bullet[] ShootBullet()
    {
        List<Bullet> bullets = new List<Bullet>();
        
        for(int i=0;i< bulletSpawningPos.Length; i++) {//TODO: considerare la rotazione in bulletOffset
            Vector3 bulletOffset = GetBulletOffset(i);
            GameObject ins =Instantiate(bullet, transform.position+bulletOffset, Quaternion.identity);
            Bullet b= ins.GetComponent<Bullet>();
            b.pattern = bulletPattern;
            b.bulletSpeed = bulletSpeed;
            b.playerorenemy = "Enemy";
            b.shootingEnemy = this;
            b.bulletDamage = bulletDamage;
            bullets.Add(b);
        }
        return bullets.ToArray();
    }
#endregion

    protected virtual void StartMoving() {
        if (path != null) {
            iTween.MoveTo(gameObject, iTween.Hash("path", path, "movetopath", false, "speed", speed,"easetype",iTween.EaseType.easeInOutSine,"oncomplete","Die"));
        }
        else {
            MoveLinear();
        }
        StartShooting();
    }

    void MoveLinear() {
        float ang = 0;
        rb.velocity = Quaternion.AngleAxis(ang, Vector3.forward) * Vector2.down * speed;
    }

    private void Update()
    {
        if (transform.position.y < -GameManager.fieldHeight / 2)
        {
            Die();
        }
    }

    public Vector2 Velocity() {
        return rb.velocity;
    }

    public void TakeDamage(float DamageTaken) {
        health -= DamageTaken;
        GameManager.instance.CurrHealth -= DamageTaken;

        Debug.Log("Nemico colpito, la vita adesso è " + health);
        if (health <= 0) {
            Invoke("Die", 0.05f);
            Instantiate(explosion, transform.position, Quaternion.identity);
            GameManager.instance.Score += scoreValue;
            Debug.Log("Vita minore di 0");
        }
        else {
            audio.pitch = Random.Range(0.93f, 1.07f);
            audio.panStereo = transform.position.x / (GameManager.width * 3);
            audio.Play();
        }

    }

    protected virtual void Die() {
        Destroy(gameObject);
        GameManager.instance.CurrMaxHealth -= maxHealth;
        if (health > 0) GameManager.instance.CurrHealth -= health;
        if (transform.parent != null) {//Va a implementare questo solo se è stato generato da una wave
            GetComponentInParent<WaveWithEnemies>().StillAlive--;
        }
    }

}
