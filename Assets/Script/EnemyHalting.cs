﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHalting : Enemy {
    //Parametri configurazione movimento
    [HideInInspector]
    public Transform target;
    public float holdingTime;
    [HideInInspector]
    public int slotIdInWave;
    // Use this for initialization

        
    new protected void Start () {
        speed = 3f;
        shootDelay = 1f;
        bulletPattern = Bullet.BulletPattern.linear;
        bulletSpeed = -4.0f;
        shootType = ShootType.burst;
        scoreValue = 100f;
        maxHealth = 40f;
        base.Start();
    }
    

    new void StartShooting()
    {
        //base.StartShooting();
        Invoke("Leave", holdingTime);
        StartCoroutine("ShootBurst");

    }

    
    IEnumerator ShootBurst() {
        float posSign = Mathf.Sign(transform.position.x);
        Vector2 corner = new Vector3(GameManager.width * posSign, -GameManager.height)-transform.position;
        Debug.Log(corner);
        float startAng= Vector2.SignedAngle(Vector2.down, corner);
        //transform.rotation = Quaternion.AngleAxis(startAng, Vector3.forward);
        iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(0, 0, startAng), "time", 0.1f,"easetype",iTween.EaseType.easeInSine));
        yield return new WaitForSeconds(0.1f);

        while (true) {
            for (int j = 0; j < 10; j++) {

                for (int i = 0; i < numBullet; i++) {
                    Bullet b = ShootBullet()[0];//TODO: Correggere per gestire più colpi contemporaneamente
                    b.customAngle =startAng + posSign*(-burstAngle / 2 + burstAngle / numBullet * i);
                    yield return new WaitForSeconds(0.05f);
                }
                startAng -= 30 * posSign;
                iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(0, 0, startAng), "time", shootDelay, "easetype", iTween.EaseType.easeInOutSine));

                yield return new WaitForSeconds(shootDelay);

                //transform.rotation = Quaternion.AngleAxis(startAng, Vector3.forward);

                        }

        }
    }

    protected override void StartMoving() {
        iTween.MoveTo(gameObject, iTween.Hash("position", target, "speed", speed, "easetype", iTween.EaseType.easeInOutSine, "oncomplete", "StartShooting"));
    }

    protected override void Die() {
        base.Die();
        GetComponentInParent<WaveHalting>().SpawnInSlot(slotIdInWave);
    }

    void Leave() {
        StopAllCoroutines();//Facciamo smettere di sparare il nemico

        Vector3 leavingTarget = new Vector3(GameManager.width * 1.3f * Mathf.Sign(transform.position.x), transform.position.y);
        iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(0, 0, Vector2.SignedAngle(Vector2.down,leavingTarget)), "time", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        iTween.MoveTo(gameObject, iTween.Hash("position", leavingTarget, "speed", speed, "easetype", iTween.EaseType.easeInOutSine, "oncomplete", "Die"));
    }

    /*
private void OnCollisionEnter2D(Collision2D collision)
{
   if (collision.tag == "Player")
   {

   }
}*/
}
