﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour {

    public int waveId;


    public void EndWave() {
        GameManager.instance.SpawnNextWave();
        Destroy(gameObject);
    }
}
