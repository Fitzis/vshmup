﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy {
    Collider2D[] colliders;
    int numPhaseTot = 2;
    int currPhase = 0;

    public GameObject shield;
    Animator shieldAnimator;
	// Use this for initialization
	new void Start () {
        speed = 5;
        shootDelay = .5f;
        bulletPattern = Bullet.BulletPattern.wave;
        bulletSpeed = -4.0f;
        shootType = ShootType.radial;
        scoreValue = 100f;
        maxHealth = 40f;
        base.Start();
        colliders = GetComponents<Collider2D>();
        shieldAnimator = shield.GetComponent<Animator>();
    }

    protected override void StartMoving() {
        iTween.MoveTo(gameObject, iTween.Hash("path", path, "speed", speed, "easetype", iTween.EaseType.easeInOutSine, "movetopath",false, "oncomplete", "StartNextPhase"));
    }

    void StartNextPhase() {
        if (currPhase < numPhaseTot) {
            Invoke("Phase" + (currPhase + 1),0);//TODO: riguardare
            currPhase ++;
        }
        else {
            GetComponentInParent<Wave>().EndWave();
        }
    }

    IEnumerator Move() {
        while (true) {
            iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("MovementPhase1"), "time", 10f, "easetype", iTween.EaseType.easeInOutSine));
            yield return new WaitForSeconds(30);
        }
    }

    IEnumerator Shoot() {
        int i=0;
        while (true) {
            StopShooting();
            ChangeShootingPreset(i);
            i++;
            StartShooting();
            yield return new WaitForSeconds(10f);
        }
    }

    void ChangeShootingPreset(int choice) {
        choice = choice % 3;//Qui va il numero di diverse stance.
        switch (choice) {
            case 0:
                shootDelay = 1f;
                bulletPattern = Bullet.BulletPattern.homing;
                bulletSpeed = -4.0f;
                shootType = ShootType.single;
                break;
            case 1:
                shootDelay = 3f;
                bulletPattern = Bullet.BulletPattern.linear;
                bulletSpeed = -8.0f;
                numBullet = 16;
                shootType = ShootType.burst;
                burstAngle = 90;
                break;
            case 2:
                shootDelay = .3f;
                bulletPattern = Bullet.BulletPattern.homing;
                bulletSpeed = -4.0f;
                shootType = ShootType.single;
                break;
        }
    }

    void Phase1() {
        maxHealth = 40f;
        StartCoroutine("Move");
        StartCoroutine("Shoot");
    }

    void Phase2() {
        maxHealth = 60f;
        SetHealth();
        StartShooting();
        colliders[0].enabled = false;
        colliders[1].enabled = true;
        shieldAnimator.SetTrigger("destructing");
        Destroy(shield, shieldAnimator.GetCurrentAnimatorClipInfo(0).Length);
        //transform.GetChild(0).gameObject.SetActive(false);
    }

    protected override void Die() {
        StartNextPhase();
        GameManager.instance.CurrMaxHealth -= maxHealth;
        if (health > 0) GameManager.instance.CurrHealth -= health;
        //GetComponentInParent<Wave>().StillAlive--;
        Debug.Log("Override del Die");

    }



    /*
    IEnumerator Phase2() {

    }*/

}
