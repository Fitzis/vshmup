﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour {

    public delegate void OnEnd();
    public static event OnEnd OnEndEvent;

    private Text UIText;
    private Animator textBoxAnimator;

    [TextArea]
    public string[] textToShow;

    int currString=-1;//Viene portato a 0 in NextDialog

    [Range(1.0f,10f)]
    public float textSpeed;

    public AudioClip startDialogueFx;
    public AudioClip transmissionFx;

    public Image barrelroll;

    bool diagActive=false;//Per sapere se il dialogo è attivo
    bool isCompleted = false;//per sapere se la riga corrente è stata mostrata tutta

    private void Awake() {

        GameObject Textbox = GameObject.Find("TextBox");
        textBoxAnimator = Textbox.GetComponent<Animator>();
        UIText = Textbox.GetComponent<Image>().rectTransform.GetChild(0).GetComponent<Text>();
        barrelroll=Textbox.GetComponent<Image>().rectTransform.GetChild(1).GetComponent<Image>();

        currString = -1;
        UIText.text = "";
        OnEndEvent = null;//resettiamo l'evento per svuotarlo da precedenti subscribe

        //UIText = Textbox.transform.GetChild(0).GetComponent<Text>();
    }




    private void Update() {
        if (Input.GetButtonDown("Fire1") && diagActive && currString!=-1) {
            if (!isCompleted) {
                UIText.text = textToShow[currString];
                isCompleted = true;
                StopAllCoroutines();
            }
            else {
                NextDialogue();
            }
        }        
    }

    public void StartDialogue() {
        textBoxAnimator.SetBool("Open", true);
        diagActive = true;
        Player.active = false;//Non facciamo prendere i movimenti del player

        MusicPlayer.instance.PlaySound(startDialogueFx);

        Invoke("NextDialogue", textBoxAnimator.GetCurrentAnimatorStateInfo(0).length) ;//Diamo il tempo al box di arrivare in posizione prima di far partire il dialogo
    }

    void NextDialogue() {
        if (currString < textToShow.Length-1) {
            currString++;
            StartCoroutine("DisplayText");
            MusicPlayer.instance.PlaySound(transmissionFx);
            if (currString == 2) {
                barrelroll.gameObject.SetActive(true);
            }
            isCompleted = false;
        }
        else {
            EndDialogue();
        }
    }

    IEnumerator DisplayText() {
        UIText.text = "";
        foreach (char c in textToShow[currString].ToCharArray()) {

            UIText.text += c;
            if (c.Equals('\n')){
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(1/(10*textSpeed));
        }
        isCompleted = true;
    }

    public void EndDialogue() {
        diagActive = false;
        Player.active = true;//Il giocatore può ricominciare a muovere
        barrelroll.gameObject.SetActive(false);
        MusicPlayer.instance.StopSound();

        textBoxAnimator.SetBool("Open", false);
        if (OnEndEvent != null) {
            OnEndEvent();

        }
    }

}
