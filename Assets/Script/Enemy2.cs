﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Enemy {
    
	// Use this for initialization
	new void Start () {
        speed = 1f;
        shootDelay = 2f;
        bulletPattern = Bullet.BulletPattern.spiral;
        shootType = ShootType.radial;
        scoreValue = 200f;
        bulletSpeed = -1.0f;
        base.Start();       
	}
	
}
