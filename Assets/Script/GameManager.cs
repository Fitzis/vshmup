﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public GameObject endingPanel;
    public static float fieldHeight;//width e height della sprite di sfondo
    public static float fieldWidth;
    public static float width;//width e height della camera
    public static float height;

    private float score;
    public float Score {
        get { return score; }
        set {
            score = value;
            scoreText.text = "SCORE:\n" + score;
        }
    }
    public Text scoreText;

    public SpriteRenderer sr;

    #region Salute dei Nemici
    //Barra salute dei nemici
    public Slider enemyHealthBar;
    RectTransform healthBar;
    float maxWidth;
    float currMaxHealth;
    float currHealth;

    public float CurrMaxHealth {
        get { return currMaxHealth; }
        set {
            currMaxHealth = value;
            healthBar.anchorMax = new Vector2(currMaxHealth/maxWidth,1);
        }
    }
    public float CurrHealth {
        get { return currHealth; }
        set {
            currHealth = value;
            enemyHealthBar.value= currHealth / currMaxHealth;
        }
    }
    #endregion


    public GameObject[] Waves;//wave spawnabili
    int currentWaveId=-1;

    void Start () {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Inizializzazioni variabili
        fieldHeight = sr.bounds.size.y;
        fieldWidth = sr.bounds.size.x;
        //Debug.Log("la width è" + fieldWidth);

        Camera cam = GetComponent<Camera>();
        height = cam.orthographicSize;
        width = height * cam.aspect;

        Debug.Log("La width della camera è " + width + "la height è " + height);
        healthBar = enemyHealthBar.GetComponent<RectTransform>();
        maxWidth = healthBar.parent.GetComponent<RectTransform>().sizeDelta.x;
        Debug.Log(maxWidth);

        SpawnNextWave();//Chiamiamo la prima Wave
    }
	
    public void SpawnNextWave() {
        currentWaveId++;
        if (currentWaveId < Waves.Length) {
            Instantiate(Waves[currentWaveId], Vector2.zero, Quaternion.identity,transform);
        }
        else {
            endingPanel.SetActive(true);
            endingPanel.GetComponent<RectTransform>().GetChild(0).GetComponent<Text>().text = "Hai vinto!";
            Debug.Log("HAI VINTO! HAI FINITO TUTTE LE WAVE!");
        }
    }

}
