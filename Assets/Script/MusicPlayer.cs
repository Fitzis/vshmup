﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {
    public static MusicPlayer instance;
    AudioSource sfx;
	void Awake () {
        if (instance == null) {
            instance = this;
        }
        else if (instance!=this)
            {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        sfx = GetComponents<AudioSource>()[1];
	}

    public void PlaySound(AudioClip clip, float volume=-1) {
        sfx.clip = clip;
        sfx.Play();
        if (volume != -1) {
            sfx.volume = volume;
        }
    }

    public void StopSound() {
        sfx.Stop();
    }
}
