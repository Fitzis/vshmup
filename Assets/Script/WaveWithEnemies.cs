﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveWithEnemies : Wave {

    public int numEnemy;
    private int numSimultaneousEnemy;
    public float delay;

    public GameObject enemyPrefab;

    iTweenPath[] paths;
    //Parametri dei nemici appartenenti a questa Wave
    public float speed = 5f;
    public float shootDelay = 1f;
    public Bullet.BulletPattern bulletPattern= Bullet.BulletPattern.linear;
    public float bulletSpeed = -4.0f;
    public Enemy.ShootType shootType = Enemy.ShootType.burst;
    public float scoreValue = 100f;
    public float maxHealth = 40f;

    int stillAlive;

    public int StillAlive {
        get { return stillAlive; }
        set {
            stillAlive = value;
            if (stillAlive == 0) {
                EndWave();
            }
        }
    }

	// Use this for initialization
	protected void Start () {

        //Prendiamo i path
        paths = GetComponents<iTweenPath>();
        numSimultaneousEnemy = Mathf.Max(paths.Length,1);//se non ce ne sono mettiamo 1

        stillAlive = numEnemy * numSimultaneousEnemy;
        StartCoroutine("SpawnEnemy");


    }

    IEnumerator SpawnEnemy() {
        for(int i = 0; i < numEnemy; i++) {
            for(int j = 0; j < numSimultaneousEnemy; j++) {
                GameObject ins = Instantiate(enemyPrefab, new Vector2(Random.Range(-3, 3), GameManager.height + 5), Quaternion.identity, transform);
                Enemy enemy = ins.GetComponent<Enemy>();
                enemy.path = iTweenPath.GetPath(paths[j].pathName);
                SetValuesOnEnemy(enemy);
            }
            yield return new WaitForSeconds(delay);
        }
    }

    void SetValuesOnEnemy(Enemy enemy) {
        enemy.speed = speed;
        enemy.shootDelay = shootDelay;
        enemy.bulletPattern = bulletPattern;
        enemy.bulletSpeed = bulletSpeed;
        enemy.shootType = shootType;
        enemy.scoreValue = scoreValue;
        enemy.maxHealth = maxHealth;
    }
}
