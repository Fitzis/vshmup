﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGenerator : MonoBehaviour {
    public GameObject[] planets;
    // Use this for initialization
    float width;
    float height;
    void Start() {
        width = GameManager.width;
        height = GameManager.height;
        StartCoroutine("SpawnPlanet");

    }

    IEnumerator SpawnPlanet() { 
        while (true){
            yield return new WaitForSeconds(Random.Range(15, 35));
            GameObject ins= Instantiate(planets[Random.Range(0, planets.Length)], new Vector2(Random.Range(-width, +width), Random.Range(1.4f * height, 1.6f * height)), Quaternion.identity);
            ins.GetComponent<MovingObject>().speed = Random.Range(0.5f, 0.7f);
            float factor = Random.Range(0.3f, 0.5f);
            ins.transform.localScale = new Vector3(factor, factor, 0);
            ins.transform.rotation = Quaternion.AngleAxis(Random.Range(-90f, 90f), Vector3.forward);

        }
    }
}

