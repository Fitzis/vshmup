﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
    public AudioMixer mixer;
    public Slider Sfxslider;
    public Slider musicSlider;

    private void Start() {
        float f;
        mixer.GetFloat("sfxVolume", out f);
        Sfxslider.value = f;
        mixer.GetFloat("musicVolume", out f);
        musicSlider.value = f;
    }

    public void StartScene(string SceneName) {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneName);
    }

    public void EndGame() {
        Application.Quit();
    }

    public void SetSfxVolume(float volume) {
        mixer.SetFloat("sfxVolume", volume);
    }

    public void SetMusicVolume(float volume) {
        mixer.SetFloat("musicVolume", volume);
    }
}
