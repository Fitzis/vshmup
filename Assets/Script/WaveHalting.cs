﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveHalting : WaveWithEnemies {

    Transform[] targets;
    int numSlot;
    int[] enemyPerSlot;
	// Use this for initialization
	new void Start () {
        
        GetPositions();
        numSlot = targets.Length;
        enemyPerSlot = new int[numSlot];
        for(int i = 0; i < numSlot; i++) {
            enemyPerSlot[i] = numEnemy;
            SpawnInSlot(i);
        }

        StillAlive = numEnemy * numSlot;
    }
    
    public void SpawnInSlot(int slotId) {
        if (enemyPerSlot[slotId] > 0) {
            GameObject ins = Instantiate(enemyPrefab, new Vector2(targets[slotId].position.x, GameManager.height + 2), Quaternion.identity, transform);
            EnemyHalting e = ins.GetComponent<EnemyHalting>();
            e.target = targets[slotId];
            e.slotIdInWave = slotId;
            enemyPerSlot[slotId]--;
        }
    }

    void GetPositions() {
        Transform[] pos= GetComponentsInChildren<Transform>();
        List<Transform> t = new List<Transform>();
        for(int i = 0; i < pos.Length; i++) {
            if (pos[i] != transform) {
                t.Add(pos[i]);
            }
        }
        targets = t.ToArray();
    }
}
