﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingBackground : MonoBehaviour {
    Rigidbody2D rb;
    float height;
    public float speed;
    // Use this for initialization
    void Start () {
        height = GameManager.fieldHeight;
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.down*speed;
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -height)
        {
            transform.Translate(Vector2.up * height * 2);
        }
	}
}
