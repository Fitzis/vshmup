﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour, IDamageable {

    public static Player instance;
    public static bool active;

    public GameObject endingPanel;
    //Variabili di lavoro
    Rigidbody2D rb;
    AudioSource audio;
    Animator animator;
    public GameObject bullet;
    float elapsed = 0;
    float shipHeight;

    bool invulnerable=false;
    public float invulnerableTime = 1.0f;

    //Parametri configurazione navetta
    public float speed;
    float shootDelay = 0.1f;
    float bulletDamage = 10f;
    float maxHealth = 100f;
    float health;  
    float Health {
        get {return health;}
        set {
            health = value;
            healthIndicator.value = health / maxHealth;
        }
    }
    bool mouseEnabled = false;
    //UI
    public Slider healthIndicator;
	

	void Start () {

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        rb = GetComponent<Rigidbody2D>();
        health = maxHealth;
        shipHeight = GetComponent<SpriteRenderer>().bounds.size.y;
        audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
        if (!active) {
            return;
        }

        elapsed += Time.deltaTime;
        rb.AddForce(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed);
        if (Input.GetButton("Fire1") && elapsed>=shootDelay)
        {
            ShootBullet();
            elapsed = 0;
        }
        if (Input.GetMouseButtonDown(2)) { mouseEnabled = !mouseEnabled; }
        if(mouseEnabled) rb.AddForce((Camera.main.ScreenToWorldPoint(Input.mousePosition) - Player.instance.transform.position)*100);
    }

    void ShootBullet()
    {
        GameObject ins = Instantiate(bullet, transform.position+transform.up*shipHeight/2, Quaternion.identity);
        Bullet b = ins.GetComponent<Bullet>();
        b.playerorenemy = "Player";
        b.bulletDamage = bulletDamage;
        audio.pitch = Random.Range(0.93f, 1.07f);
        audio.panStereo = transform.position.x / (GameManager.width*3);
        audio.Play();
    }

    public void TakeDamage(float damageTaken)
    {
        if (invulnerable) {
            return;
        }
        Health-=damageTaken;

        SetInvulnerable();
        Debug.Log(Health);
        if (Health <= 0)
        {
            Destroy(gameObject);
            Debug.Log("Game Over!");
            endingPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void SetInvulnerable() {
        invulnerable = true;
        Invoke("ResetInvulnerable", invulnerableTime);
        animator.SetBool("Invulnerable", true);
    }

    void ResetInvulnerable() {
        invulnerable = false;
        animator.SetBool("Invulnerable", false);
    }



    private void OnCollisionEnter2D(Collision2D collision) {//Quando incontriamo un altro collider
        if (collision.gameObject.tag == "Enemy") {
            TakeDamage(20f);
        }
    }
}
