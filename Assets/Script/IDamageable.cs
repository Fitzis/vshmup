﻿public interface IDamageable {

    void TakeDamage(float DamageTaken);

}
