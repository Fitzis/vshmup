﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    //Variabili di lavoro
    Animator bullAnim;
    Rigidbody2D rb;
    public Enemy shootingEnemy;//Qui è salvato chi ha sparato il proiettile (se è un nemico)

    //Parametri configurazione proiettile
    public enum BulletPattern {linear,wave,homing,target,spiral};
    public BulletPattern pattern;
    public float bulletSpeed;
    public string playerorenemy;//il tag sarà Player o Enemy per capire se è un proiettile amico o nemico
    public float bulletDamage;

    float maxHomingAngle = 2f;
    public float customAngle; //Questo parametro è per decidere una direzione


    //Parametri di configurazione del seno
    float frequency = 2f;
    float amplitude = 1f;

    //Parametri di configurazione della spirale
    private float k=1.5f; //ampiezza

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        switch (pattern)
        {
            case BulletPattern.linear:
                 SetVelocity(Vector2.up * bulletSpeed);
               // StartCoroutine("MoveLinear");
                break;
            case BulletPattern.target:
                //rb.velocity = GetVectorTowardPlayer() * bulletSpeed;
                StartCoroutine("MoveTarget");
                break;
            case BulletPattern.homing:
                rb.velocity = GetVectorTowardPlayer() * bulletSpeed;
                StartCoroutine("MoveHoming");
                break;
            case BulletPattern.wave:
                StartCoroutine("MoveWaving");
                break;
            case BulletPattern.spiral:
                StartCoroutine("MoveSpiral");
                break;
        }
        if (playerorenemy == "Enemy")//Cambiamo il colore dei proiettili nemici
        {
            GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);

        }

        bullAnim = GetComponent<Animator>();
    }



    IEnumerator MoveTarget() {
        rb.velocity = -Vector2.down * bulletSpeed;
        yield return new WaitForSeconds(0.15f);
        Vector3 playerPos= Player.instance.transform.position;
        while ((rb.velocity.normalized - (Vector2)(transform.position - playerPos).normalized).sqrMagnitude > 1f) {
            rb.velocity = Vector3.Lerp(rb.velocity, (transform.position -playerPos).normalized * bulletSpeed, 5f*Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
        }

    }
    
    /*
    IEnumerator MoveHoming()
    {
        float totCurvature = 0;
        rb.velocity = -Vector2.down * bulletSpeed;//Il proiettile parte verso il basso
        yield return new WaitForSeconds(0.05f);//per i primi 0.05 secondi
        while (totCurvature<180)//Il proiettile insegue il giocator solo finché la somma della curva che ha fatto è minore di 180
        {
            float ang = Vector2.SignedAngle(rb.velocity, -1*GetVectorTowardPlayer());//angolo tra velocità attuale e direzione verso il giocatore
            //Inseriamo un fattore di smorzamento maxHomingAngle, in modo da non far seguire esattamente il giocatore
            ang = Mathf.Min(Mathf.Abs(ang), maxHomingAngle) * Mathf.Sign(ang);
            Quaternion rot = Quaternion.AngleAxis(ang, Vector3.forward);
            rb.velocity = rot * rb.velocity;//ruotiamo il vettore velocità
            totCurvature += Mathf.Abs(ang);
            yield return new WaitForFixedUpdate();
        }
    }*/

    IEnumerator MoveHoming() {
        float elapsed = 0;
        rb.velocity = -Vector2.down * bulletSpeed;//Il proiettile parte verso il basso
        yield return new WaitForSeconds(0.15f);//per i primi 0.05 secondi
        while (elapsed<2f)
        {
            float ang = Vector2.SignedAngle(rb.velocity, -1 * GetVectorTowardPlayer());//angolo tra velocità attuale e direzione verso il giocatore
            Quaternion rot = Quaternion.AngleAxis(ang, Vector3.forward);
            rb.velocity = rot * rb.velocity;//ruotiamo il vettore velocità
            elapsed += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MoveWaving()
    {
        float t = 0;
        while (true)
        {
            t += Time.deltaTime;
            SetVelocity( new Vector2(amplitude* frequency* Mathf.Cos(frequency* bulletSpeed* t), 1) * bulletSpeed);
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MoveSpiral()
    {
        rb.velocity = -Vector2.down * bulletSpeed;
        yield return new WaitForSeconds(0.15f);
        float t = 0;
        while (true)
        {
            t += Time.deltaTime;

            SetVelocity(new Vector2(k * Mathf.Cos(t)-k*t*Mathf.Sin(t),k*Mathf.Sin(t)+k*t*Mathf.Cos(t)));
            yield return new WaitForFixedUpdate();
        }
    }

    void SetVelocity(Vector2 velocity)
    {
        rb.velocity = velocity;
        if (customAngle != null)//Se vogliamo sparare in direzioni custom
        {
            rb.velocity = Quaternion.AngleAxis(customAngle, Vector3.forward) * rb.velocity;
        }
        if (shootingEnemy != null) {//Se ha sparato un nemico aggiungiamo la sua velocità
            rb.velocity += shootingEnemy.Velocity();
        }
    }

    Vector2 GetVectorTowardPlayer() { return (transform.position - Player.instance.transform.position).normalized; }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (playerorenemy=="Player" && collision.tag == "Enemy")
        {
            //Destroy(gameObject);
            collision.GetComponent<IDamageable>().TakeDamage(bulletDamage);
            TurnInExplosion(collision);
        }
        else if (playerorenemy == "Enemy" && collision.tag == "Player")
        {
            //Destroy(gameObject);
            collision.gameObject.GetComponent<IDamageable>().TakeDamage(bulletDamage);
            TurnInExplosion(collision);

        }
    }

    private void TurnInExplosion(Collider2D collision) {
        transform.SetParent(collision.transform);
        bullAnim.SetBool("hasHit", true);
        rb.velocity = Vector2.zero;
        Destroy(gameObject, bullAnim.GetCurrentAnimatorStateInfo(0).length);
        transform.localScale = new Vector3(Random.Range(0.85f, 1.15f), Random.Range(0.85f, 1.15f), 0);
        transform.rotation = Quaternion.AngleAxis(Random.Range(-90f, 90f), Vector3.forward);
        this.enabled = false;

    }

    private void Update()
    {
        //Ruotiamo il proiettile perché segua la direzione del movimento
        float ang = Vector2.SignedAngle(Vector2.down,rb.velocity);
        transform.rotation = Quaternion.AngleAxis(ang,Vector3.forward);

        //Distruggiamo il proiettile se esce dal campo
        if(Mathf.Abs(transform.position.y) > GameManager.height*1.1|| Mathf.Abs(transform.position.x) > GameManager.fieldWidth*1.1) {
            Destroy(gameObject);
        }
    }
}
