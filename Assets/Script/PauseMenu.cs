﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : Menu {
    public static bool isPaused=false;
    public GameObject pauseMenu;
    public AudioClip pauseSound;
    public AudioClip unpauseSound;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Start")) {
            if (isPaused) {
                Resume();
            }
            else { Pause(); }
        }
	}

    void Pause() {
        isPaused = true;
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
        MusicPlayer.instance.PlaySound(pauseSound);

    }

    public void Resume() {
        isPaused = false;
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        MusicPlayer.instance.PlaySound(unpauseSound);

    }
}
