﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavePause : Wave {

    public float waitingTime=0.0f;
    private void Start() {
        Dialogue d = GetComponent<Dialogue>();
        if (d != null) {
            d.StartDialogue();
            Dialogue.OnEndEvent += EndWave;
        }
        else {
            Invoke("EndWave", waitingTime);
        }
    }
    

}
