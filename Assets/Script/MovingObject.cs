﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour {
    public float speed;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -GameManager.height*1.5) {
            Destroy(gameObject);
        }
	}
}
